import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nowrap'
})

export class Nowrap implements PipeTransform {
  public transform(value, args: string) {
    if(!value) {
      return;
    }
    if(value.length > 211){
      var div = document.createElement("div");
      div.innerHTML = value;
      let string =  div.innerText.substr(0, 208);
      return string + "...";
    }
    return value;
  }
}
